package com.lee.log.job;

import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author LIJIYANG001
 * @date 2021/4/26 18:21
 */
@Component
public class GenerateLog {
    private static final String INDEX_NAME = "iot_ts_log-alias";
    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateLog.class);

    @Autowired
    private RestHighLevelClient client;

    @Scheduled(fixedRate = 10*1000)
    public void log() {
        BulkRequest request = new BulkRequest();
        String id = System.currentTimeMillis() + "_" + new Random().nextInt(1000);
        request.add(new IndexRequest(INDEX_NAME).id(id)
                .source(XContentType.JSON,"content", generateString(), "ctime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
        try {
            BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            LOGGER.error("log() err:", e);
        }
    }

    private String generateString(){
        return "this log generate at:" + new SimpleDateFormat("yyyyMMddHHmmss");
    }

}
